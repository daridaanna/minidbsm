import entities.Attribute;
import entities.CreateTableResponse;
import entities.ServerResponse;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.*;

public class TableController {

    private ClientConnection clientConnection;

    private String currentDatabase;

    private boolean primaryKey = false;

    private Map<String, Attribute> attributeMap;

    @FXML
    ComboBox<String> databaseChoiceB;

    @FXML
    AnchorPane anchorPaneTable;

    @FXML
    ListView<String> attributes;

    @FXML
    Button _addAttribute;

    @FXML
    TextField _nameOfAttribute;

    @FXML
    TextField _length;

    @FXML
    ChoiceBox<String> _typeOfAttribute;

    @FXML
    CheckBox _isNullCB;

    @FXML
    ChoiceBox<String> _indexCB;

    @FXML
    CheckBox FKCB;

    @FXML
    ComboBox<String> referencesCB;

    @FXML
    Label messageLabel;

    @FXML
    TextField _tableName;

    @FXML
    ComboBox<String> tableDropCombo;

    @FXML
    public void initialize() {
        attributeMap = new HashMap<>();
        clientConnection = new ClientConnection("127.0.0.2", 5000);
        _typeOfAttribute.setItems(FXCollections.observableArrayList(
                "VARCHAR", "INT", "TEXT", "DATE")
        );

        _typeOfAttribute.setValue("VARCHAR");

        _indexCB.setItems(FXCollections.observableArrayList("", "UNIQUE", "PRIMARY KEY", "INDEX"));

        setUpDatabaseNames();

        //get tables based on database name
        setUpTableNames();

    }

    public void dropTable() {
        if (tableDropCombo.getSelectionModel().isEmpty()) messageLabel.setText("Choose table to drop");
        else {
            messageLabel.setText(clientConnection.sendCommandToServer("DROP TABLE " + currentDatabase + " " +
                    tableDropCombo.getSelectionModel().getSelectedItem()));
            setUpTableNames();
        }
    }

    private void setUpDatabaseNames() {
        ServerResponse databaseResponse = clientConnection.getListOfDatabaseNames();
        if (databaseResponse.isSuccessful()){
            List<String> namesFromServer = databaseResponse.getServerNames();
            for(String name: namesFromServer){
                databaseChoiceB.getItems().add(name);
            }
            databaseChoiceB.setValue(namesFromServer.get(0));
            currentDatabase = namesFromServer.get(0);
        } else{
            messageLabel.setText(databaseResponse.getMessage());
        }
    }

    private void setUpTableNames() {
        referencesCB.getItems().clear();
        tableDropCombo.getItems().clear();

        ServerResponse databaseResponse = clientConnection.getTableNames(currentDatabase);
        if (databaseResponse.isSuccessful()){
            List<String> namesFromServer = databaseResponse.getServerNames();
            if (namesFromServer.get(0).equals("0")) messageLabel.setText("No tables in this database.");
            else {
                for(String name: namesFromServer){
                    referencesCB.getItems().add(name);
                    tableDropCombo.getItems().add(name);
                }
            }
        } else{
            messageLabel.setText(databaseResponse.getMessage());
        }
    }

    public void changeDatabase(){
        System.out.println("Database was changed to: " + databaseChoiceB.getValue());
        currentDatabase = databaseChoiceB.getValue();
        setUpTableNames();
    }

    public void goBackToDatabase(){
        try {
            AnchorPane tablePane = FXMLLoader.load(getClass().getResource("databaseManagement.fxml"));
            anchorPaneTable.getChildren().setAll(tablePane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addAttribute() {
        boolean canAdd = true;
        boolean isNull = _isNullCB.isSelected();

        String nameOfAttribute = _nameOfAttribute.getText();
        String typeOfAttribute = _typeOfAttribute.getValue();
        String lengthText = _length.getText();
        String referenceTable = "";
        String referenceAttribute = "";
        int length = 255;
        String index = "";
        String message = "";


        if (FKCB.isSelected()) {
            if (referencesCB.getSelectionModel().isEmpty()) {
                message += "Select reference table for foreign key.";
                canAdd = false;
            } else {
                referenceTable = referencesCB.getSelectionModel().getSelectedItem();
            }
        }

        if (nameOfAttribute.length()==0) {
            canAdd = false;
            message += "Name the attribute \n";
        }

        if (attributeMap.containsKey(nameOfAttribute)) {
            canAdd = false;
            message += "Attribute name already exists \n";
        }

        if (lengthText.length() != 0) {
            if(lengthText.matches("[0-9]+")){
                length = Integer.parseInt(lengthText);
            } else {
                canAdd = false;
                message += "Length field can only contain numbers. \n";
            }
        }

        if (_indexCB.getValue().length()>0) index = _indexCB.getValue();

        if (canAdd){
            boolean fk = FKCB.isSelected();
            Attribute attribute;

            boolean isIndex = (index.length()>1);

            if (fk) attribute = new Attribute(nameOfAttribute, typeOfAttribute, length, isNull, index, fk,
                    referenceTable, referenceAttribute);
            else attribute = new Attribute(nameOfAttribute, typeOfAttribute, length, isNull, index, isIndex);

            if (attribute.isPrimaryKey()) primaryKey = true;
            attributeMap.put(nameOfAttribute, attribute);

            attributes.getItems().add("Name:" + nameOfAttribute + " - " + "Type:" + typeOfAttribute + " - " +
                    "Length: " + length + "  -  " + "Is Null: " + isNull + "  -  " + "Index:" + index + " - " + "Foreign key:" +
                    fk + " - " + "Reference Table:" + referenceTable + " - " + "Reference Attribute:" +
                    referenceAttribute);}
        else messageLabel.setText(message);

        referencesCB.getSelectionModel().clearSelection();
        _isNullCB.setSelected(false);
        FKCB.setSelected(false);
        _indexCB.setValue("");

    }

    public void removeAttribute(){
        String toBeRemoved = attributes.getSelectionModel().getSelectedItem();
        System.out.println("To be removed:" + toBeRemoved);
        int indexToBeRemoved = attributes.getSelectionModel().getSelectedIndex();

        if (toBeRemoved.isEmpty())
            messageLabel.setText("Select attribute to be removed!");
        else {
            String[] row = toBeRemoved.split(" ");
            String[] result = row[0].split(":");
            System.out.println("nameOfAttribute: " + result[1] + "/");

            if (attributeMap.get(result[1]).isPrimaryKey()) primaryKey = false;

            attributeMap.remove(result[1]);
            attributes.getItems().remove(indexToBeRemoved);
        }
    }

    public void createTable(){
        boolean canCreate = true;
        String errorMessage = "";
        String tableName = _tableName.getText();

        if(tableName.isEmpty()){
            canCreate = false;
            errorMessage += "Can't create table without table name. \n";
        }

        if (!primaryKey){
            errorMessage += "Add primary key attribute. \n";
        }

        if (attributeMap.size() == 0) {
            canCreate = false;
            errorMessage += "Add attributes to create table. \n";
        }

        if (canCreate) {
            String createMessage = "CREATE TABLE " + "databaseName=" + currentDatabase + "-" +
                    "tableName=" + tableName + "-";

            Map<String, Attribute> copyOfMap = new HashMap<>(attributeMap);
            Iterator it = copyOfMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                Attribute attr = (Attribute) pair.getValue();
                createMessage += attr.toString() + "-";
                it.remove();
            }
            System.out.println("Client: " + createMessage);

            CreateTableResponse createTableResponse = clientConnection.createTable(createMessage);
            System.out.println("Create table RESPONSE= " + createTableResponse.toString());
            if(createTableResponse.isSuccessful()) {
                attributes.getItems().clear(); // clear listView
                attributeMap.clear();
                setUpTableNames();
                _tableName.clear();
                messageLabel.setText(createTableResponse.getMessage());
            } else {
                messageLabel.setText(createTableResponse.getMessage());
            }

        } else {
            messageLabel.setText(errorMessage);
        }
    }

    public void goToIndexManagement(){
        try {
            AnchorPane tablePane = FXMLLoader.load(getClass().getResource("indexManagement.fxml"));
            anchorPaneTable.getChildren().setAll(tablePane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
