package entities;

import java.util.List;

public class Index {
    private String indexName;
    private String indexType;
    private int keyLength;
    private boolean isUnique;
    private List<String> attributes;

    public Index(String indexName, String indexType, int keyLength, boolean isUnique, List<String> attributes) {
        this.indexName = indexName;
        this.indexType = indexType;
        this.keyLength = keyLength;
        this.isUnique = isUnique;
        this.attributes = attributes;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public int getKeyLength() {
        return keyLength;
    }

    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(String attr) {
        this.attributes.add(attr);
    }

    @Override
    public String toString() {
        String attributesString = "[";
        for(String attr: attributes) {
            attributesString += attr + ",";
        }
        attributesString += "]";

        return "indexName=" + indexName + "-" +
                "indexType=" + indexType + "-" +
                "keyLength=" + keyLength + "-" +
                "isUnique=" + isUnique + "-" +
                "attributes=" + attributesString;
    }
}
