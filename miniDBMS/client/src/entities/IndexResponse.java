package entities;

import java.util.*;

public class IndexResponse {
    private boolean isSuccessful;
    private String message;
    private Map<String, Index> indexes;
    public boolean isEmpty;

    public IndexResponse(boolean isSuccessful, String toBeProcessed) {
        System.out.println("Index response trying to process: " + toBeProcessed);
        this.isSuccessful = isSuccessful;
        indexes = new HashMap<>();

        if(toBeProcessed.equals("0")) {
            isEmpty = true;
        } else {
            String[] indexFiles = toBeProcessed.split(";");
            for (String indexFileString : indexFiles) {
                String indexName = "";
                int keyLength = 0;
                boolean isUnique = false;
                String indexType = "";
                List<String> attributes = new LinkedList<>();
                String[] indexKeys = indexFileString.split("#");
                for (String keyValue : indexKeys) {
                    String[] split = keyValue.split("=");
                    switch (split[0]) {
                        case "indexName":
                            indexName = split[1];
                            break;
                        case "keyLength":
                            keyLength = Integer.parseInt(split[1]);
                            break;
                        case "isUnique":
                            isUnique = (Integer.parseInt(split[1]) == 1);
                            break;
                        case "indexType":
                            indexType = split[1];
                            break;
                        case "attributes":
                            if(!split[1].equals("[]")) {
                                String arrayString = split[1].substring(1, split[1].length() - 2);
                                attributes.addAll(Arrays.asList(arrayString.split(",")));
                            }
                    }
                }
                Index index = new Index(indexName, indexType, keyLength, isUnique, attributes);
                indexes.put(indexName, index);
            }
        }
    }

    public IndexResponse(String message) {
        this.isSuccessful = false;
        this.message = message;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Index> getIndexes() {
        return indexes;
    }

    public void setIndexes(Map<String, Index> indexes) {
        this.indexes = indexes;
    }

    public void addAttribute(String indexName, Index index) {
        indexes.put(indexName, index);
    }

    public boolean indexExists(String nameOfNewIndex) {
        return indexes.containsKey(nameOfNewIndex);
    }
}
