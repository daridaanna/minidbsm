package entities;

public class Attribute {
    private String attributeName;
    private String type;
    private int length;
    private boolean isNull;
    private boolean isIndex;
    private String index;
    private boolean isPrimaryKey;
    private boolean isForeignKey;
    private String referenceTable;
    private String referenceAttribute;

    public Attribute(String attributeName, String type, int length, boolean isNull, String index, boolean isIndex) {
        this.attributeName = attributeName;
        this.type = type;
        this.length = length;
        this.isNull = isNull;
        this.index = index;
        isPrimaryKey = index.equals("PRIMARY KEY");
        isForeignKey = false;
        this.isIndex = isIndex;
        referenceTable = "";
        referenceAttribute = "";
    }

    public Attribute(String attributeName, String type, int length, boolean isNull, String index,
                     boolean isForeignKey, String referenceTable, String referenceAttribute) {
        this.attributeName = attributeName;
        this.type = type;
        this.length = length;
        this.isNull = isNull;
        this.index = index;
        this.isIndex = false;
        this.isPrimaryKey = false;
        this.isForeignKey = isForeignKey;
        this.referenceTable = referenceTable;
        this.referenceAttribute = referenceAttribute;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    @Override
    public String toString() {
        int isNullInt;

        if (isNull) isNullInt = 1;
        else isNullInt = 0;
        return "attributeName=" + attributeName + '#' +
                "type=" + type + '#' +
                "length=" + length + '#' +
                "isNull=" + isNullInt + '#' +
                "isIndex=" + isIndex +"#" +
                "index=" + index + '#' +
                "isPrimaryKey=" + isPrimaryKey + '#' +
                "isForeignKey=" + isForeignKey + '#' +
                "referenceTable=" + referenceTable + '#'
                + "referenceAttribute=" + referenceAttribute;
    }
}
