package entities;

import java.util.List;

public class ServerResponse {
    private String message;
    private boolean isSuccessful;
    private List<String> serverNames;

    public ServerResponse(List<String> databaseNames) {
        this.isSuccessful = true;
        this.serverNames = databaseNames;

    }

    public ServerResponse(String message){
        isSuccessful = false;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public List<String> getServerNames() {
        return serverNames;
    }

    public void setServerNames(List<String> serverNames) {
        this.serverNames = serverNames;
    }
}
