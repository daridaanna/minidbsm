package entities;

public class CreateTableResponse {
    private boolean isSuccessful;
    private String message;

    public CreateTableResponse(String message) {
        System.out.println("Create table response: " + message);
        String[] split = message.split("#");
        this.isSuccessful = split[0].split("=")[1].equals("true");
        this.message = split[1].split("=")[1];
    }

    public CreateTableResponse(boolean isSuccessful, String message) {
        this.isSuccessful = isSuccessful;
        this.message = message;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CreateTableResponse{" +
                "isSuccessful=" + isSuccessful +
                ", message='" + message + '\'' +
                '}';
    }
}
