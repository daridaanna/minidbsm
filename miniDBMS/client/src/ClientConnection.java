
import entities.CreateTableResponse;
import entities.IndexResponse;
import entities.ServerResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ClientConnection {
    private Socket socket = null;
    private String address;
    private int tcp;
    private PrintStream outputStream = null; //writing to server
    private BufferedReader inputStream = null; //reading from server

    public ClientConnection(String address, int tcp) {
        this.address = address;
        this.tcp = tcp;
    }

    public String createDropDatabase(String choice, String name) {
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println(choice + " DATABASE " + name); //send database action

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            System.out.println(message);
            return message;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "No message";
    }

    public ServerResponse getListOfDatabaseNames(){
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println("GET DATABASES");

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            if (message.length()>0) {
                return databaseResponse(message, "databaseNames");
            } else {
                return new ServerResponse("No message from server.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ServerResponse("Error occurred on client side");
    }

    private ServerResponse databaseResponse(String fromServer, String name){
        String[] splitter = fromServer.split("#");
        List<String> list = new LinkedList<>();
        if (splitter.length == 2){
            String isSuccessful = splitter[0];
            if (isSuccessful.contains(":")){
                String[] mess = isSuccessful.split(":");
                if (mess[0].equals("isSuccessful")) {
                    if (mess[1].equals("true")) {
                        if (splitter[1].contains(":")) {
                            String[] listResults = splitter[1].split(":");
                            if (listResults[0].equals(name)){
                                String[] names = listResults[1].split(" ");
                                list.addAll(Arrays.asList(names));
                                return new ServerResponse(list);
                            }
                        }
                    } else {
                        // Server sent problem
                        String[] messageArray = splitter[1].split(":");
                        if (messageArray[0].equals("message")) return new ServerResponse(messageArray[1]);
                    }
                }
            }
        }
        return new ServerResponse("Can't process server's message");
    }

    public String sendCommandToServer(String sqlCommand) {
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println(sqlCommand);

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            System.out.println(message);
            return message;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "No message";
    }

    public ServerResponse getTableNames(String databaseName) {
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println("GET TABLES " + databaseName);

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            if (message.length()>0) {
                return databaseResponse(message, "tableNames");
            } else {
                return new ServerResponse("No message from server.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ServerResponse("Error occurred on client side");
    }

    public ServerResponse getAttributeNames(String tableName, String databaseName) {
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println("GET ATTRIBUTES " + databaseName + " " + tableName);

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            if (message.length()>0) {
                return databaseResponse(message, "attributeNames");
            } else {
                return new ServerResponse("No message from server.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ServerResponse("Error occurred on client side");
    }

    public IndexResponse getIndexNames(String dbName, String tableName) {
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println("GET INDEXES " + dbName + " " + tableName);

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            if (message.length()>0) {
                return new IndexResponse(true, message);
            } else {
                return new IndexResponse("No message from server.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new IndexResponse("Error occurred on client side");
    }

    public CreateTableResponse createTable(String createTableString) {
        try {
            socket = new Socket(address, tcp);
            System.out.println("Connected to Server");

            outputStream = new PrintStream(socket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            outputStream.println(createTableString);

            String message = inputStream.readLine();
            inputStream.close();
            socket.close();
            outputStream.close();

            if (message.length()>0) {
                return new CreateTableResponse(message);
            } else {
                return new CreateTableResponse(false, "No message from server.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new CreateTableResponse(false, "Error occurred on client side");
    }

}

