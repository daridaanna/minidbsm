import entities.Attribute;
import entities.Index;
import entities.IndexResponse;
import entities.ServerResponse;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class IndexController {

    private ClientConnection clientConnection;

    private String currentDatabaseName;

    private String currentTableName;

    private IndexResponse indexResponse;

    private List<String> newAttributes;

    @FXML
    AnchorPane indexAnchor;

    @FXML
    ComboBox<String> _databaseComboBox;

    @FXML
    ComboBox<String> _tableComboBox;

    @FXML
    ListView<String> _indexListView;

    @FXML
    Label _messageDropIndexLabel;

    @FXML
    Label _messageCreateIndexLabel;

    @FXML
    ListView<String> _createIndexListView;

    @FXML
    ComboBox<String> _addAttributeComboBox;

    @FXML
    TextField _indexNameTextField;

    @FXML
    ChoiceBox<String> _indexTypeChoiceBox;

    @FXML
    public void initialize() {
        clientConnection = new ClientConnection("127.0.0.1", 5000);
        newAttributes = new LinkedList<>();
        _indexTypeChoiceBox.setItems(FXCollections.observableArrayList("UNIQUE","INDEX"));

        //set up database comboBox
        setUpDatabaseComboBox();
    }

    public void goToTableManagement() {
        try {
            AnchorPane tablePane = FXMLLoader.load(getClass().getResource("tableManagement.fxml"));
            indexAnchor.getChildren().setAll(tablePane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void gotToDatabaseManagement() {
        try {
            AnchorPane tablePane = FXMLLoader.load(getClass().getResource("databaseManagement.fxml"));
            indexAnchor.getChildren().setAll(tablePane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addAttribute() {
        String newAttribute = _addAttributeComboBox.getSelectionModel().getSelectedItem();
        if (newAttribute.isEmpty())
            _messageCreateIndexLabel.setText("In order to add attribute you need to select one.");
        else {
            String attr = _addAttributeComboBox.getSelectionModel().getSelectedItem();
            if(newAttributes.contains(attr)) {
                _messageCreateIndexLabel.setText("Attribute has been added already.");
            } else {
                newAttributes.add(attr);
                _createIndexListView.getItems().add(newAttribute);
            }
        }
    }

    private void setUpDatabaseComboBox() {
        ServerResponse serverResponse = clientConnection.getListOfDatabaseNames();
        if (serverResponse.isSuccessful()) {
            _databaseComboBox.setItems(FXCollections.observableArrayList(serverResponse.getServerNames()));
        } else {
            _messageDropIndexLabel.setText(serverResponse.getMessage());
        }
    }

    public void setUpTableNames() { //when db was chosen
        _tableComboBox.getItems().clear();
        currentDatabaseName = _databaseComboBox.getSelectionModel().getSelectedItem();
        ServerResponse serverResponse = clientConnection.getTableNames(currentDatabaseName);

        if (serverResponse.isSuccessful()) {
            if(serverResponse.getServerNames().get(0).equals("0"))
                _messageDropIndexLabel.setText("Chosen database has no tables.");
            else
                _tableComboBox.setItems(FXCollections.observableArrayList(serverResponse.getServerNames()));
        } else {
            _messageDropIndexLabel.setText(serverResponse.getMessage());
        }
    }

    public void setUpAttributeNames() {
        _addAttributeComboBox.getItems().clear();
        currentTableName = _tableComboBox.getSelectionModel().getSelectedItem();
        ServerResponse serverResponse = clientConnection.getAttributeNames(currentTableName, currentDatabaseName);
        _addAttributeComboBox.setItems(FXCollections.observableArrayList(serverResponse.getServerNames()));
    }

    public void setUpIndexNames() {
        setUpAttributeNames();
        _indexListView.getItems().clear();
        currentTableName = _tableComboBox.getSelectionModel().getSelectedItem();
        indexResponse = clientConnection.getIndexNames(currentDatabaseName, currentTableName);

        if (indexResponse.isEmpty) _messageDropIndexLabel.setText("Chosen table has no index files.");
        else {
            Iterator it = indexResponse.getIndexes().entrySet().iterator();
            while (it.hasNext()) {
                String indexRow = "";
                Map.Entry pair = (Map.Entry) it.next();
                Index index = (Index) pair.getValue();
                indexRow += index.toString();
                _indexListView.getItems().add(indexRow);
                it.remove();
            }
        }
    }

    public void dropIndex() {
        if (_indexListView.getItems().isEmpty()) _messageDropIndexLabel.setText("Choose index to drop");
        else {
            String toBeDropped = _indexListView.getSelectionModel().getSelectedItem();
            String nameOfIndex = toBeDropped.split("-")[0].split("=")[1];
            System.out.println("Name of index to be dropped: " + nameOfIndex);
            _indexListView.getItems().remove(toBeDropped);
            _messageDropIndexLabel.setText(clientConnection.sendCommandToServer("DROP INDEX " +
                    currentDatabaseName + " " + currentTableName + " " + nameOfIndex));
        }
    }

    public void createIndex() {
        boolean canCreate = true;
        String message = "";
        String fromTextField = _indexNameTextField.getText();
        String indexType = _indexTypeChoiceBox.getValue();
        String attributesList = "";

        if(fromTextField.isEmpty()) {
            canCreate = false;
            message += "Name the index you want to create. \n";
        }

        if (indexResponse.indexExists(fromTextField + ".ind")) {
            canCreate = false;
            message += "This index name already exists. Choose another one. \n";
        }

        if (newAttributes.size() == 0) {
            canCreate = false;
            message += "In order to create an index you need to add attributes. \n";
        } else {
            for (String attr: newAttributes) {
                attributesList += attr + ",";
            }
        }

        if (indexType.isEmpty()) {
            canCreate = false;
            message += "Choose index type. \n";
        }

        if (canCreate) {
            _messageCreateIndexLabel.setText(clientConnection.sendCommandToServer("CREATE INDEX " +
                    currentDatabaseName + " " +
                    currentTableName + " " +
                    "indexName=" + fromTextField + "#indexType=" + indexType +
                    "#attributes=" + attributesList));
            newAttributes.clear();
            _createIndexListView.getItems().clear();
            setUpIndexNames();
        } else {
            _messageCreateIndexLabel.setText(message);
        }
    }
}
