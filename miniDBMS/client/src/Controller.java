import entities.ServerResponse;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Controller {

    private ClientConnection clientConnection;

    private List<String> databases;

    @FXML
    public AnchorPane anchorPane;

    @FXML
    private Label _messageLabel;

    @FXML
    private ComboBox<String> _databaseComboBox;

    @FXML
    private TextField _databaseNameTextField;

    @FXML
    public void initialize() {
        clientConnection = new ClientConnection("127.0.0.1", 5000);
        databases = new LinkedList<>();

        ServerResponse serverResponse = clientConnection.getListOfDatabaseNames();
        if(!serverResponse.isSuccessful())
            _messageLabel.setText(serverResponse.getMessage());
        else {
            if (serverResponse.getServerNames().size() == 0) {
                _messageLabel.setText("No databases is system yet.");
            } else {
                _databaseComboBox.setItems(FXCollections.observableArrayList(serverResponse.getServerNames()));
                databases = serverResponse.getServerNames();
            }
        }
    }

    private void updateDatabases() {
        _databaseComboBox.getItems().clear();
        _databaseComboBox.setItems(FXCollections.observableArrayList(databases));
    }

    public void createDatabase() {
        String fromTextField = _databaseNameTextField.getText();
        if (fromTextField.isEmpty()) _messageLabel.setText("Name the new database");
        else {
            if (databases.contains(fromTextField)) _messageLabel.setText("A database with this name already exists.");
            else {
                _messageLabel.setText(clientConnection.sendCommandToServer("CREATE DATABASE " +
                        fromTextField));
                databases.add(fromTextField);
                updateDatabases();
            }
        }
    }

    public void dropDatabase() {
        String fromComboBox = _databaseComboBox.getSelectionModel().getSelectedItem();
        if (fromComboBox.isEmpty()) _messageLabel.setText("Choose a database to drop.");
        else {
            System.out.println("Droping: " + fromComboBox);
            databases.remove(fromComboBox);
            _messageLabel.setText(clientConnection.sendCommandToServer("DROP DATABASE " + fromComboBox));
        }
    }


    public void goToTables(){
        try {
            AnchorPane tablePane = FXMLLoader.load(getClass().getResource("tableManagement.fxml"));
            anchorPane.getChildren().setAll(tablePane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goToIndexManagement() {
        try {
            AnchorPane tablePane = FXMLLoader.load(getClass().getResource("indexManagement.fxml"));
            anchorPane.getChildren().setAll(tablePane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

