package entities;

import java.util.LinkedList;
import java.util.List;

public class Table {
    private String tableName;
    private String fileName;
    private int rowLength;
    private List<Attribute> structure;
    private List<PrimaryKey> primaryKey;
    private List<ForeignKey> foreignKeys;
    private List<IndexFile> indexFiles;
    private List<UniqueKey> uniqueKeys;

    public Table(String tableName) {
        this.tableName = tableName;
        this.fileName = tableName + ".kv";
        primaryKey = new LinkedList<>();
        foreignKeys = new LinkedList<>();
        structure = new LinkedList<>();
        this.rowLength = 0;
        indexFiles = new LinkedList<>();
        uniqueKeys = new LinkedList<>();
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getRowLength() {
        return rowLength;
    }

    public void setRowLength(int rowLength) {
        this.rowLength = rowLength;
    }

    public List<Attribute> getStructure() {
        return structure;
    }

    public void setStructure(List<Attribute> structure) {
        this.structure = structure;
    }

    public List<PrimaryKey> getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKeys(List<PrimaryKey> primaryKeys) {
        this.primaryKey = primaryKeys;
    }

    public List<ForeignKey> getForeignKeys() {
        return foreignKeys;
    }

    public void setForeignKeys(List<ForeignKey> foreignKeys) {
        this.foreignKeys = foreignKeys;
    }

    public void addPrimaryKey(PrimaryKey pk){
        primaryKey.add(pk);
    }

    public void addAttribute(Attribute attribute) {
        structure.add(attribute);
    }

    public void addForeignKey(String attributeName, String refTable, String refAttribute) {
        foreignKeys.add(new ForeignKey(attributeName, new Reference(refTable, refAttribute)));
    }

    public List<String> getAttributes() {
        List<String> attributes = new LinkedList<>();
        for(Attribute attr: structure) {
            attributes.add(attr.getAttributeName());
        }
        return attributes;
    }

    public void setPrimaryKey(List<PrimaryKey> primaryKey) {
        this.primaryKey = primaryKey;
    }

    public List<IndexFile> getIndexFiles() {
        return indexFiles;
    }

    public void setIndexFiles(List<IndexFile> indexFiles) {
        this.indexFiles = indexFiles;
    }

    public List<UniqueKey> getUniqueKeys() {
        return uniqueKeys;
    }

    public void setUniqueKeys(List<UniqueKey> uniqueKeys) {
        this.uniqueKeys = uniqueKeys;
    }

    public void addIndexFileWithAttribute(String attrName, boolean isUnique, int keyLength) {
        int unique = 0;
        if (isUnique) unique = 1;

        IndexFile indexFile = new IndexFile(attrName, keyLength, unique);
        indexFile.addIndexAttribute(attrName);

        System.out.println("INDEX=" + indexFile.toString());
        indexFiles.add(indexFile);
        System.out.println("Index file has been added");
    }

    public void addIndexFile(IndexFile indexFile) {
        indexFiles.add(indexFile);
    }

    public String indexFilesToClient() {
        String result = "";
        boolean hasIndexFile = false;

        for (IndexFile indexFile : indexFiles) {
            hasIndexFile = true;
            String attributeList = "[";
            for(String attr: indexFile.getIndexAttribute()) {
                attributeList += attr + ",";
            }
            attributeList += "]";

            result += "indexName=" + indexFile.getIndexName() + "#keyLength=" + indexFile.getKeyLength() +
                    "#isUnique=" + indexFile.getIsUnique() +"#indexType=" + indexFile.getIndexType() +
                    "#attributes=" + attributeList + ";";
        }

        if (!hasIndexFile) result = "0";
        System.out.println("Table sending list of index files: " + result);
        return result;
    }

    public String dropIndex(String indexName) {
        indexFiles.removeIf(indexFile -> indexFile.getIndexName().equals(indexName));
        return "Index was successfully removed";
    }

    public String createIndex(String name, String type, List<String> attributes) {
        int keyLength = 0;
        int isUnique = 0;
        for(Attribute attribute: structure){
            if (attributes.contains(attribute.getAttributeName())) keyLength += attribute.getLength();
        }

        if (type.equals("UNIQUE")) isUnique = 1;
        IndexFile indexFile = new IndexFile(name, keyLength, isUnique);

        for(String attr: attributes) {
            indexFile.addIndexAttribute(attr);
        }
        System.out.println("New index file: " + indexFile.toString());
        indexFiles.add(indexFile);
        return "Index has been successfully added.";
    }

    public Attribute getAttribute(String attrName) {
        for(Attribute attr: structure) {
            if(attr.getAttributeName().equals(attrName)) return attr;
        }
        return null;
    }

    public void addUniqueKey(UniqueKey uk) {
        uniqueKeys.add(uk);
    }

    @Override
    public String toString() {
        String indexes = "";
        for(IndexFile indexFile: indexFiles) {
            indexes += indexFile.toString() + "\n";
        }

        String foreignKeyMess = "";
        for(ForeignKey foreignKey: foreignKeys) {
            foreignKeyMess += foreignKey.toString() + "\n";
        }

        return "Table{" +
                "tableName='" + tableName + '\'' +
                ", fileName='" + fileName + '\'' +
                ", rowLength=" + rowLength +
                ", structure=" + structure.toString() +
                ", primaryKeys=" + primaryKey.toString() +
                ", index=" + indexes + "\n" +
                "foreign keys= " + foreignKeyMess +
                '}';
    }
}
