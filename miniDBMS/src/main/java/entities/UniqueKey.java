package entities;

public class UniqueKey {
    private String uniqueAttribute;

    public UniqueKey(String uniqueAttribute) {
        this.uniqueAttribute = uniqueAttribute;
    }

    public String getUniqueAttribute() {
        return uniqueAttribute;
    }

    public void setUniqueAttribute(String uniqueAttribute) {
        this.uniqueAttribute = uniqueAttribute;
    }

}
