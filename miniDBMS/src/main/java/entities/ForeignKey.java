package entities;

public class ForeignKey {
    private String fkAttribute;
    private Reference reference;

    public ForeignKey(String fkAttribute, Reference reference) {
        this.fkAttribute = fkAttribute;
        this.reference = reference;
    }

    public String getFkAttribute() {
        return fkAttribute;
    }

    public void setFkAttribute(String fkAttribute) {
        this.fkAttribute = fkAttribute;
    }

    public Reference getReference() {
        return reference;
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
        return "ForeignKey{" +
                "fkAttribute='" + fkAttribute + '\'' +
                ", reference=" + reference.toString() +
                '}';
    }
}
