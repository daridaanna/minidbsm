package entities;

public class Reference {
    private String refTable;
    private String refAttribute;

    public Reference(String refTable, String refAttribute) {
        this.refTable = refTable;
        this.refAttribute = refAttribute;
    }

    public String getRefTable() {
        return refTable;
    }

    public void setRefTable(String refTable) {
        this.refTable = refTable;
    }

    public String getRefAttribute() {
        return refAttribute;
    }

    public void setRefAttribute(String refAttribute) {
        this.refAttribute = refAttribute;
    }

    @Override
    public String toString() {
        return "Reference{" +
                "refTable='" + refTable + '\'' +
                ", refAttribute='" + refAttribute + '\'' +
                '}';
    }
}
