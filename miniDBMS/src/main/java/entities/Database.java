package entities;

import java.util.*;

public class Database {

    private String databaseName;
    private List<Table> tables;

    public Database() {
        this.databaseName = "";
        tables = new ArrayList<>();
    }

    public Database(String name) {
        this.databaseName = name;
        tables = new ArrayList<>();
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    public Table getTable(String tableName) {
        for(Table table: tables){
            if (table.getTableName().equals(tableName)) return table;
        }
        return null;
    }

    public void setTable(Table table) {
        tables.add(table);
    }

    public void removeTable(String tableName) {
        tables.removeIf(table -> table.getTableName().equals(tableName));
    }

    @Override
    public String toString() {
        String message = "";

        for(Table table: tables){
            message += table.toString() + "\n";
        }

        return "Database{" +
                "databaseName='" + databaseName + '\'' +
                ", tables=" + message +
                '}';
    }
}
