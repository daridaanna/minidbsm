package entities;

import java.util.LinkedList;
import java.util.List;

public class Structure {

    private List<Attribute> attributes;

    public Structure(){
        attributes = new LinkedList<>();
    }

    public Attribute getAttribute(String attributeName) {
        for(Attribute attribute: attributes) {
            if (attribute.getAttributeName().equals(attributeName)) return attribute;
        }
        return null;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public void setAttribute(Attribute attribute) {
        attributes.add(attribute);
    }

}
