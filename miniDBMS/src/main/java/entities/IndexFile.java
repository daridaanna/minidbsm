package entities;

import java.util.LinkedList;
import java.util.List;

public class IndexFile {
    private String indexName;
    private int keyLength;
    private int isUnique;
    private String indexType;
    private List<String> indexAttribute;

    public IndexFile(String indexName, int keyLength, int isUnique) {
        System.out.println("Index being created: " + indexName + " keylength: " + keyLength + " isUnique: " + isUnique);
        if(indexName.contains(".ind"))
            this.indexName = indexName;
        else
            this.indexName = indexName + ".ind";
        this.keyLength = keyLength;
        this.isUnique = isUnique;
        this.indexType = "BTree";
        indexAttribute = new LinkedList<>();
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public int getKeyLength() {
        return keyLength;
    }

    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }

    public int getIsUnique() {
        return isUnique;
    }

    public void setIsUnique(int isUnique) {
        this.isUnique = isUnique;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public List<String> getIndexAttribute() {
        return indexAttribute;
    }

    public void setIndexAttribute(List<String> indexAttribute) {
        this.indexAttribute = indexAttribute;
    }

    public void addIndexAttribute(String name) {
        indexAttribute.add(name);
    }

    public void removeIndexAttribute(String name) {
        indexAttribute.removeIf(index -> index.equals(name));
    }

    @Override
    public String toString() {
        String attributesString = "[";
        for (String atr: indexAttribute) {
            attributesString += atr +',';
        }
        attributesString += "]";

        return "IndexFile{" +
                "indexName='" + indexName + '\'' +
                ", keyLength=" + keyLength +
                ", isUnique=" + isUnique +
                ", indexType='" + indexType + '\'' +
                ", indexAttribute=" + attributesString +
                '}';
    }
}
