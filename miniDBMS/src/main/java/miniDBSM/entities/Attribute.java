package miniDBSM.entities;

public class Attribute {
    private String attributeName;
    private String type;
    private int length;
    private int isNull;

    public Attribute(String attributeName, String type, int length, int isNull) {
        this.attributeName = attributeName;
        this.type = type;
        this.isNull = isNull;
        this.length = length;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getIsNull() {
        return isNull;
    }

    public void setNull(int isNull) {
        isNull = isNull;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "attributeName='" + attributeName + '\'' +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", isNull=" + isNull +
                '}';
    }
}
