package miniDBSM.entities;


import java.util.*;

public class Databases {
    private List<Database> databases;

    public Databases() {
        databases = new ArrayList<>();
    }

    public List<Database> getDatabases() {
        return databases;
    }

    public void setDatabases(List<Database> databases) {
        this.databases = databases;
    }

    public void addDatabase(Database db) {
        databases.add(db);
    }

    public void deleteDatabase(String db) {
        databases.removeIf(database -> database.getDatabaseName().equals(db));
    }

    public boolean databaseExists(String name){
        for(Database db: databases){
            if (db.getDatabaseName().equals(name)) return true;
        }
        return false;
    }

    public Database getDatabase(String dbName) {
        for(Database db: databases) {
            if (db.getDatabaseName().equals(dbName)) return db;
        }
        return null;
    }

    @Override
    public String toString() {
        String message = "";

        for(Database db: databases){
            message += db.toString() + "\n";
        }

        return "Databases{" +
                "databases=" + message +
                '}';
    }
}
