package miniDBSM;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import miniDBSM.entities.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class Server {

    private boolean databasesExist = false;
    private File databasesJson;
    private Databases databases;
    private ObjectMapper objectMapper;

    public Server() {
        try {
            databasesJson = new File("src/main/resources/DataBases.json");
            databases = new Databases();
            objectMapper = new ObjectMapper();

            if (databasesJson.createNewFile()) {
                System.out.println("DataBases.json was created.");
            } else {
                databasesExist = true;
                //read in memory database.json
                readInDatabaseJson();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void readInDatabaseJson(){
        try{
            BufferedReader reader = Files.newBufferedReader(Paths.get("src/main/resources/DataBases.json"));

            JsonNode parser = objectMapper.readTree(reader);

            for (JsonNode database : parser.path("databases")) {
                Database db = new Database(database.path("databaseName").asText());

                for(JsonNode table : database.path("tables")) {
                    String tableName = table.path("tableName").asText();
                    Table tableToBe = new Table(tableName);

                    String fileName = table.path("fileName").asText();
                    tableToBe.setFileName(fileName);

                    int rowLength = table.path("rowLength").asInt();
                    tableToBe.setRowLength(rowLength);

                    //get attributes
                    for (JsonNode attribute : table.path("structure")) {
                        Attribute attributeRead = new Attribute(attribute.path("attributeName").asText(),
                                attribute.path("type").asText(), attribute.path("length").asInt(),
                                attribute.path("isNull").asInt());
                        tableToBe.addAttribute(attributeRead);
                    }

                    //get primary key(s)
                    for(JsonNode pkAttr: table.path("primaryKey")){
                        tableToBe.addPrimaryKey(new PrimaryKey(pkAttr.path("pkAttribute").asText()));
                    }

                    //get foreign keys
                    for(JsonNode fkAttr: table.path("foreignKeys")) {
                        JsonNode reference = fkAttr.path("reference");
                        tableToBe.addForeignKey(fkAttr.path("fkAttribute").asText(), reference.path("refTable").asText(),
                                reference.path("refAttribute").asText());
                    }

                    //get indexFiles
                    for(JsonNode indexFile: table.path("indexFiles")) {
                        IndexFile indexFile1 = new IndexFile(indexFile.path("indexName").asText(),
                                indexFile.path("keyLength").asInt(), indexFile.path("isUnique").asInt());
                        for (JsonNode indexAttribute: indexFile.path("indexAttribute")) {
                            indexFile1.addIndexAttribute(indexAttribute.asText());
                        }
                        tableToBe.addIndexFile(indexFile1);
                    }

                    db.setTable(tableToBe);
                }
                databases.addDatabase(db);
            }

            System.out.println("Databases in system: " + databases.toString());

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private String createDatabase(String nameOfDatabase){
            if (!databasesExist){ //if system has no existing database
                databases = new Databases();
                databases.addDatabase(new Database(nameOfDatabase));
            } else {
                if (databases.databaseExists(nameOfDatabase)) return "A database with this name already exists! \n ";

                databases.addDatabase(new Database(nameOfDatabase));
            }
            writeToJson();
            return "Database " + nameOfDatabase + " was successfully created!";
    }

    private void writeToJson() {
        try{
            objectMapper.writeValue(databasesJson, databases);
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    private String dropDatabase(String nameOfDb){
        if(databases.databaseExists(nameOfDb)){
            databases.deleteDatabase(nameOfDb);
            writeToJson(); // write to disk

            return "Database " + nameOfDb + " has been successfully deleted!";
        } else {
            return "No database exists in this system with this name.";
        }
    }

    private Attribute stringToAttribute(String attrString){
        String [] attrSplit = attrString.split("#");

        String attributeName = "";
        String type = "";
        int length = 250;
        int isNull = 0;

        for (String pair : attrSplit) {
            String[] help = pair.split("=");
            switch(help[0]) {
                case "attributeName":
                    attributeName = help[1];
                    break;
                case "type":
                    type = help[1];
                    break;
                case "length":
                    length = Integer.parseInt(help[1]);
                    break;
                case "isNull":
                    isNull = Integer.parseInt(help[1]);
                    break;
            }
        }

        return new Attribute(attributeName, type, length, isNull);
    }

    //ToDo: implement creating table with primary key, foreign key, etc.
    private String createTable(String command){
        System.out.println("Create table command: " + command);
        String[] commandSplit = command.split("-");

        String databaseName = commandSplit[0].split("=")[1];

        Table table = new Table(commandSplit[1].split("=")[1]); //get name of table

        for(int i = 2; i<commandSplit.length; i++) {
            String com = commandSplit[i];

            Attribute newAttribute = stringToAttribute(com);

            if (com.matches("(.*)isPrimaryKey=true(.*)"))
                table.addPrimaryKey(new PrimaryKey(newAttribute.getAttributeName()));

            if (com.matches("(.*)isForeignKey=true(.*)")) {
                String fkName = newAttribute.getAttributeName();
                String refAttr = "";
                String refTab = "";
                String[] split = com.split("#");
                for(String pair: split) {
                    if(pair.matches("referenceTable(.*)")) {
                        refTab = pair.split("=")[1];
                    } else {
                        if (pair.matches("referenceAttribute(.*)")) refAttr = pair.split("=")[1];
                    }
                }
                Attribute match = databases.getDatabase(databaseName).getTable(refTab).getAttribute(refAttr);
                if(match.getType().equals(newAttribute.getType()))
                    table.addForeignKey(fkName, refTab, refAttr);
                else return "isSuccessful=false#message=Foreign key's type doesn't match reference table's.";
            }

            if(com.matches("(.*)isIndex=true(.*)")) {
                String[] split = com.split("#");
                String indexType = "";
                for(String pair: split) {
                    if(pair.matches("index=(.*)")) {
                        indexType = pair.split("=")[1];
                        break;
                    }
                }
                boolean isUnique = indexType.equals("UNIQUE");
                table.addIndexFileWithAttribute(newAttribute.getAttributeName(), isUnique,
                        newAttribute.getLength());
            }

            table.addAttribute(newAttribute);
        }

        for (Database database : databases.getDatabases()) {
            if (database.getDatabaseName().equals(databaseName)) {
                database.setTable(table);
            }
        }

        writeToJson();
        System.out.println("Table created: " + table.toString());
        return "isSuccessful=true#message=Table " + table.getTableName() + " was successfully created!";
    }

    private String dropTable(String databaseName, String tableName){
        for(Database db: databases.getDatabases()) {
            if (db.getDatabaseName().equals(databaseName)) {
                db.removeTable(tableName);
                writeToJson();
                return "Table was removed.";
            }
        }
        return "Table was not found";
    }

    private String createIndex(String dbName, String tableName, String indexInfo){
        for (Database db: databases.getDatabases()) {
            if (db.getDatabaseName().equals(dbName)) {
                String indexName = ""; String type=""; List<String> attributeName = new LinkedList<>();
                String[] pairs = indexInfo.split("#");
                for(String pair: pairs) {
                    String[] split = pair.split("=");
                    switch (split[0]){
                        case "indexName":
                            indexName = split[1];
                            break;
                        case "indexType":
                            type = split[1];
                            break;
                        case "attributes":
                            attributeName.addAll(Arrays.asList(split[1].split(",")));
                            break;
                    }
                }
                Table table = db.getTable(tableName);
                String mess = table.createIndex(indexName, type, attributeName);
                writeToJson();
                return mess;
            }
        }
        return "Error on server side";
    }

    private String getDatabases(){
        if (databases.getDatabases().size()>0){
            String result = "isSuccessful:true#databaseNames:";
            for(Database db: databases.getDatabases()) {
                result = result + db.getDatabaseName()+ " ";
            }
            System.out.println("DB result: " + result);
            return result;
        }else{
            System.out.println("isSuccessful:false#message:No databases in the system yet.");
            return "isSuccessful:false#message:No databases in the system yet.";
        }
    }

    private String getAttributes(String databaseName, String tableName) {
        System.out.println("Getting attributes: " + databaseName + " " + tableName);
        for(Database db : databases.getDatabases()) {
            if (db.getDatabaseName().equals(databaseName)) {
                String message = "isSuccessful:true#attributeNames:";
                for(Table table: db.getTables()) {
                    if (table.getTableName().equals(tableName)) {
                        for (String attrName: table.getAttributes()) {
                            message = message + attrName + " ";
                        }
                        System.out.println(message);
                        return message;
                    }
                }
            }
        }
        return "isSuccessful:false#message:Server error.";
    }

    private String getTables(String databaseName){
        for (Database db: databases.getDatabases()) {
            if (db.getDatabaseName().equals(databaseName)) { //if database was found
                StringBuilder message = new StringBuilder("isSuccessful:true#tableNames:");
                boolean tableExists = false;
                for(Table table: db.getTables()) {
                    tableExists = true;
                    message.append(table.getTableName()).append(" ");
                }

                if (!tableExists) message.append("0#");
                System.out.println("Message: " + message.toString());
                return message.toString();
            }
        }

        return "isSuccessful:false#message:Database doesn't exist.";
    }

    private String getIndexes(String dbName, String tableName) {
        for(Database db: databases.getDatabases()) {
            if (db.getDatabaseName().equals(dbName)) {
                for(Table table: db.getTables()) {
                    if (table.getTableName().equals(tableName)) {
                        return table.indexFilesToClient();
                    }
                }
            }
        }
        return "0";
    }

    private String dropIndex(String dbName, String tableName, String indexName) {
        for(Database db: databases.getDatabases()) {
            if (db.getDatabaseName().equals(dbName)) {
                return db.getTable(tableName).dropIndex(indexName);
            }
        }
        return "Error on server side.";
    }

    public static void main(String[] args) throws Exception{
        Server server = new Server();

        String sqlCommand;
        ServerSocket serverSocket = new ServerSocket(5000);
        while(true) {
            Socket socket = serverSocket.accept();
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintStream  outToClient = new PrintStream(socket.getOutputStream());

            sqlCommand = inFromClient.readLine();
            System.out.println("Sql command from client: " + sqlCommand);

            String[] sqlStatement = sqlCommand.split(" ");

            System.out.println("Sqlstatement length: " + sqlStatement.length);

            if (sqlStatement.length >= 3) {
                String command = sqlStatement[0];
                switch (command){
                    case "CREATE":
                        switch (sqlStatement[1]){
                            case "TABLE":
                                System.out.println("Sending to create table: " + sqlStatement[2] + sqlStatement[3]);
                                outToClient.println(server.createTable(sqlStatement[2]+ " " +sqlStatement[3]));
                                break;
                            case "DATABASE":
                                outToClient.println(server.createDatabase(sqlStatement[2]));
                                break;
                            case "INDEX":
                                outToClient.println(server.createIndex(sqlStatement[2], sqlStatement[3], sqlStatement[4]));
                                break;
                            default:
                                System.out.println("Wrong input from client.");
                        }
                        break;
                    case "DROP":
                        switch (sqlStatement[1]) {
                            case "TABLE":
                                outToClient.println(server.dropTable(sqlStatement[2], sqlStatement[3]));
                                break;
                            case "DATABASE":
                                outToClient.println(server.dropDatabase(sqlStatement[2]));
                                break;
                            case "INDEX":
                                outToClient.println(server.dropIndex(sqlStatement[2], sqlStatement[3], sqlStatement[4]));
                                server.writeToJson();
                                break;
                            default:
                                System.out.println("Wrong input from client.");
                        }
                        break;
                    case "GET":
                        if (sqlStatement[1].equals("TABLES")) {
                            System.out.println("Getting tables");
                            outToClient.println(server.getTables(sqlStatement[2]));
                        } else {
                            if (sqlStatement[1].equals("ATTRIBUTES")) {
                                outToClient.println(server.getAttributes(sqlStatement[2], sqlStatement[3]));
                            } else {
                                if(sqlStatement[1].equals("INDEXES")) {
                                    System.out.println("Getting indexes");
                                    outToClient.println(server.getIndexes(sqlStatement[2], sqlStatement[3]));
                                }
                            }
                        }
                        break;
                }
            } else {
                if (sqlStatement.length == 2){
                    if(sqlStatement[0].equals("GET")) {
                            if (sqlStatement[1].equals("DATABASES")){
                                outToClient.println(server.getDatabases());
                            }
                    }
                }
            }
            outToClient.close();
            inFromClient.close();
            socket.close();
        }

    }
}
